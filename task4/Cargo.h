#pragma once
class Cargo
{
private:
	float volume;
	float weight;
public:
	Cargo(float weight, float volume) {
		this->volume = volume;
		this->weight = weight;
	}

	Cargo operator + (float s) {
		this->volume += s;
		this->weight += s;
	}

	Cargo& operator ++() {
		this->volume += 1.0;
		this->weight += 1.0;
		return *this;
	}

	Cargo& operator ++ (int) {
		Cargo current = *this;
		++*this;
		return current;
	};
};

