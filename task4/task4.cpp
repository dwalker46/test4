﻿#include <iostream>
#include <vector>
#include "Cargo.h"
#include "Vehicle.h"

using namespace std;

template < class T >
void increment(T &object) {
	object++;
}

int main()
{
	vector<Cargo*> cargo;
	vector<Vehicle*> vehicle;
	vector<int> numbers(20, 10);
	cargo.push_back(new Cargo(10, 20));
	cargo.push_back(new Cargo(10, 30));
	cargo.push_back(new Cargo(20, 40));

	vehicle.push_back(new Vehicle(100, 200));
	vehicle.push_back(new Vehicle(333, 210));
	vehicle.push_back(new Vehicle(444, 120));

	auto iterCargo = cargo.begin();
	while (iterCargo != cargo.end())    
	{
		increment<Cargo>(*(*iterCargo));
		++iterCargo;            
	}
	
	auto iterVehicle = vehicle.begin();
	while (iterVehicle != vehicle.end())
	{
		increment<Vehicle>(*(*iterVehicle));
		++iterVehicle;
	}

	auto iterNumbers = numbers.begin();
	while (iterNumbers != numbers.end())
	{
		increment(*iterNumbers);
		cout << (*iterNumbers) << endl;
		++iterNumbers;
	}
}
