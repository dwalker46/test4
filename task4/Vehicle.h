#pragma once
class Vehicle
{
private:
	float volume;
	float weight;
public:
	Vehicle(float weight, float volume) {
		this->volume = volume;
		this->weight = weight;
	}
	Vehicle& operator ++ ()  {	//���������� �����
		this->volume += 1;
		this->weight += 1;
		return *this;
	};
	Vehicle& operator ++ (int) { //����������� �����
		Vehicle current = *this;
		++*this;
		return current;
	};
};

